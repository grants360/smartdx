# use small node image
FROM node:alpine

# install git ca-certificates openssl openssh for CircleCI
# install jq for JSON parsing
RUN apk add --update --no-cache git openssh ca-certificates openssl jq gettext xmlstarlet curl

# install latest sfdx from npm
RUN node --version
RUN npm install sfdx-cli --global

# install latest Vlocity Build Tool from npm
RUN npm install --global vlocity

# install latest chromium to support vlocity build tool
RUN apk update && apk add --no-cache nmap && \
	echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
	echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
	apk update && \
	apk add --no-cache \
	chromium \
	harfbuzz \
	"freetype>2.8" \
	ttf-freefont \
	nss

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
RUN npm install puppeteer -g

RUN sfdx --version
RUN vlocity help
RUN sfdx plugins --core

#salesforcedx plugin is now deprecated
#RUN sfdx plugins:install salesforcedx@latest-rc

# revert to low privilege user
USER node